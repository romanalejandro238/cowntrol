package com.example.cowntrol.ui.milk

import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import androidx.annotation.RequiresApi
import androidx.fragment.app.FragmentTransaction
import com.example.cowntrol.R
import com.example.cowntrol.ui.APIService
import com.example.cowntrol.ui.MilkCowModel
import com.example.cowntrol.ui.MilkModel
import com.example.cowntrol.ui.VacasModel
import com.example.cowntrol.ui.insemination.InseminationFragment
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter


class MilkRegister : Fragment() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.fragment_milk_register, container, false)

        if (arguments != null){
            val idVaca = requireArguments().getInt("idVaca")
            val codigoVaca = requireArguments().getString("codigoVaca")


            val editVacaId : EditText = root.findViewById(R.id.edittxtidfrm)
            editVacaId.setText(codigoVaca)

            val current = LocalDateTime.now()

            val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")

            val fechaAct = current.format(formatter)




            val ltsLeche : EditText = root.findViewById(R.id.edittxtltsfrm)
            val btnCreateUpdate : Button = root.findViewById(R.id.btnfrmUpdate)
            val btnCancel : Button = root.findViewById(R.id.btnfrmCancel)


            btnCreateUpdate.setOnClickListener(){
                actualizar(fechaAct,idVaca,ltsLeche.text.toString().toInt(),1) //comprobar que funcione
            }

            btnCancel.setOnClickListener(){
                cancelar() //listo
            }
        }


        return root
    }

    fun cancelar() {
        val fragmentMilk: Fragment = MilkFragment()
        val transaction: FragmentTransaction = requireFragmentManager().beginTransaction()
        transaction.replace(R.id.FragmentContainerView, fragmentMilk)
        transaction.addToBackStack(null)
        transaction.setReorderingAllowed(true)
        transaction.commit()
    }


    fun actualizar(fechaAct : String, idVaca : Int , ltsLeche : Int, idLeche : Int){
        //logica para actualizar litros de leche
        CoroutineScope(Dispatchers.Main).launch {
            val milkCowModel = MilkCowModel(idVaca,idLeche,ltsLeche)
            val array : ArrayList <MilkCowModel> = ArrayList<MilkCowModel> ()
            array.add(milkCowModel)
            val MilkModel = MilkModel (fechaAct,array)
            getRetrofit().create(APIService::class.java).createUpdateLeche(MilkModel); //crea o actualiza el registro de leche
            cancelar()

        }
    }

    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl("http://159.223.204.114/api/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

}