package com.example.cowntrol.ui.insemination

import android.app.DatePickerDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.FragmentTransaction
import com.example.cowntrol.R
import com.example.cowntrol.ui.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.text.SimpleDateFormat
import java.util.*

class fragment_new_insemination : Fragment() , AdapterView.OnItemSelectedListener{



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }
    lateinit var adapter : ArrayAdapter<String>
    lateinit var vacaId : String
    lateinit var arrayVacas : ArrayList<VacasModel>
    lateinit var fechaAct : String
    var btnFecha: Button? = null
    var cal = Calendar.getInstance()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val root = inflater.inflate(R.layout.fragment_new_insemination, container, false)
        val btnConfirm : Button = root.findViewById(R.id.btnfniConfirm)
        val btnCancel : Button = root.findViewById(R.id.btnfniCancel)
        val idVaca : Spinner = root.findViewById(R.id.spinnerni)
        btnFecha = root.findViewById((R.id.btnnewdatefni))

        val dateSetListener = object : DatePickerDialog.OnDateSetListener{
            override fun onDateSet(view: DatePicker, year : Int, monthOfyear : Int, dayOfMonth : Int){
                cal.set(Calendar.YEAR,year)
                cal.set(Calendar.MONTH,monthOfyear)
                cal.set(Calendar.DAY_OF_MONTH,dayOfMonth)
                updateDateInView()
            }
        }

        adapter = ArrayAdapter(requireContext(),R.layout.support_simple_spinner_dropdown_item)
        idVaca.adapter = adapter
        idVaca.onItemSelectedListener = this
        recuperaVacas()


        btnCancel.setOnClickListener(){
            salir()
        }
        btnFecha!!.setOnClickListener(){
            DatePickerDialog(requireContext(),dateSetListener,cal.get(Calendar.YEAR),cal.get(Calendar.MONTH),cal.get(Calendar.DAY_OF_MONTH)).show()
        }

        btnConfirm?.setOnClickListener{ _ ->
            confirmar()
        }


        return root
    }

    private fun confirmar() {
        CoroutineScope(Dispatchers.Main).launch {
            val vacasEnd : InseminacionModel = InseminacionModel(vacaId.toInt(),fechaAct)
            val call = getRetrofit().create(APIService::class.java).createInseminacion(vacasEnd) //recuperacion de razas
            Toast.makeText(requireContext(),"REGISTRADA CON EXITO",Toast.LENGTH_SHORT).show()
            salir()
        }
    }

    private fun selecionaFecha() {
        fechaAct = btnFecha?.text.toString()
    }

    private fun updateDateInView(){
        val myFormat = "yyyy/MM/dd"
        val sdf = SimpleDateFormat(myFormat, Locale.US)
        btnFecha!!.text = sdf.format(cal.getTime())
        selecionaFecha()

    }

    private fun recuperaVacas(){

        var call : ArrayList<VacasModel> = ArrayList<VacasModel>()
        CoroutineScope(Dispatchers.Main).launch {
            call = getRetrofit().create(APIService::class.java).searchVacas(); //recuperacion de vacas
            setData(call)

        }
    }

    private fun setData(arrayVaca : ArrayList<VacasModel>) {
        arrayVacas = arrayVaca
        if(!arrayVacas.isEmpty()){
            for(i in 1.. arrayVacas.size){
                if(arrayVacas.get(i-1).ultimaInseminacion == ""){
                    adapter.add(arrayVacas.get(i-1).codigoVaca)
                }
            }
        }
    }

    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl("http://159.223.204.114/api/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun salir() {
        val fragmentInsemination: Fragment = InseminationFragment()
        val transaction: FragmentTransaction = requireFragmentManager().beginTransaction()
        transaction.replace(R.id.FragmentContainerView, fragmentInsemination)
        transaction.addToBackStack(null)
        transaction.setReorderingAllowed(true)
        transaction.commit()
    }

    //recupera la vaca
    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, posicion: Int, p3: Long) {
        var vacaSelected = adapter.getItem(posicion)
        for (i in 1.. arrayVacas.size){
            if(arrayVacas.get(i-1).codigoVaca.equals(vacaSelected)){
                vacaId = arrayVacas.get(i-1).id.toString()
            }
        }
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {

    }

}