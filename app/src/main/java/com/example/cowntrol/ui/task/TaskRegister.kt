package com.example.cowntrol.ui.task

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.LinearLayout
import androidx.fragment.app.FragmentTransaction
import com.example.cowntrol.R
import com.example.cowntrol.ui.APIService
import com.example.cowntrol.ui.TaskModel
import com.example.cowntrol.ui.VacasModel
import com.example.cowntrol.ui.insemination.InseminationFragment
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class TaskRegister : Fragment() {

    lateinit var id : String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val root = inflater.inflate(R.layout.fragment_task_register, container, false)

        if(arguments != null){

            val tareaId : EditText = root.findViewById(R.id.edittxtidfrt)
            val tareaDescripcion : EditText = root.findViewById(R.id.edittxtdescfrt)


            id = requireArguments().getInt("id").toString()
            tareaId.setText(id.toString())
            val descripcion = requireArguments().getString("descripcion")
            tareaDescripcion.setText(descripcion)

            val btnUpdate : Button = root.findViewById(R.id.btnfrtUpdate)
            val btnCancel : Button = root.findViewById(R.id.btnfrtCancel)
            val boxFinalizada: CheckBox = root.findViewById(R.id.checkBoxfrt)


            btnUpdate.setOnClickListener(){
                if(boxFinalizada.isChecked){ //finalizada la tarea
                    actualizar()
                }

            }
            btnCancel.setOnClickListener(){
                cancelar() //listo
            }
        }
        return root
    }


    private fun cancelar() {
        val fragmentTask: Fragment = TaskFragment()
        val transaction: FragmentTransaction = requireFragmentManager().beginTransaction()
        transaction.replace(R.id.FragmentContainerView, fragmentTask)
        transaction.addToBackStack(null)
        transaction.setReorderingAllowed(true)
        transaction.commit()
    }

    private fun actualizar() {
        CoroutineScope(Dispatchers.Main).launch {
            val taskModel = TaskModel(id.toInt(),3,"")
            getRetrofit().create(APIService::class.java).updateTareas(taskModel); //actualiza el estado
            cancelar()

        }
    }


    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl("http://159.223.204.114/api/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

}