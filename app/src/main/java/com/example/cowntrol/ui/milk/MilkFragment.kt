package com.example.cowntrol.ui.milk

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.cowntrol.R
import com.example.cowntrol.ui.APIService
import com.example.cowntrol.ui.InseminacionModel
import com.example.cowntrol.ui.MilkModel
import com.example.cowntrol.ui.VacasModel
import com.example.cowntrol.ui.insemination.InseminationRegister
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MilkFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_milk, container, false)
        getMilk(root)
        return root
    }

    private fun getMilk(root : View?) {
        CoroutineScope(Dispatchers.Main).launch {
            val call : ArrayList<VacasModel> = getRetrofit().create(APIService::class.java).searchVacas(); //esta llenando de vacas no inseminaciones
            setData(call,root)

        }
    }
    private fun setData(array : ArrayList<VacasModel>, root : View?) {


        val size = array.size
        val buttonArray = ArrayList<Button>()
        val linearLayout = root?.findViewById<LinearLayout>(R.id.LinearLayoutfm)
        //ciclo para crear los botones y llenar la view de ellos
        for (i in 1..size) {
            val button = Button(requireContext())
            button.setText("Vaca #" + array.get(i-1).codigoVaca) // obtiene el id de la vaca
            button.setOnClickListener() {
                openRequestFragment(array.get(i-1))
            }
            buttonArray.add(button)
            linearLayout?.addView(buttonArray.get(i-1))


        }

    }

    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl("http://159.223.204.114/api/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun openRequestFragment(vaca : VacasModel) {

        val datos : Bundle = Bundle()
        datos.putInt("idVaca",vaca.id)
        datos.putString("codigoVaca",vaca.codigoVaca)

        val fragmentMilkRegister: Fragment = MilkRegister()
        fragmentMilkRegister.arguments = datos
        val transaction: FragmentTransaction = requireFragmentManager().beginTransaction()
        transaction.replace(R.id.FragmentContainerView, fragmentMilkRegister)
        transaction.addToBackStack(null)
        transaction.setReorderingAllowed(true)
        transaction.commit()
    }
}