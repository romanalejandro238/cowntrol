package com.example.cowntrol

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import android.widget.Toolbar
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.example.cowntrol.ui.ui.login.LoginActivity
import com.google.android.material.bottomnavigation.BottomNavigationView


class MainActivity : AppCompatActivity() {

    lateinit var idUser : String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val navView: BottomNavigationView = findViewById(R.id.nav_view)

        val navController = findNavController(R.id.FragmentContainerView)

        navView.setupWithNavController(navController)

        val toolbar : androidx.appcompat.widget.Toolbar = findViewById(R.id.toolbar)
        toolbar.setOnMenuItemClickListener(){
            when(it.itemId){
                R.id.cerrarSesion -> {
                    finish()
                    val loginActivity = Intent(this, LoginActivity::class.java)
                    startActivity(loginActivity)
                }
            }
            true
        }



        val recibir = intent
        val datos = recibir.getStringExtra("idUsuario")
        idUser = datos.toString()



        Toast.makeText(
            applicationContext,
            "ESPERE MIENTRAS SE CONECTA ",
            Toast.LENGTH_LONG
        ).show()



        val fab: View = findViewById(R.id.fabnewcow)
        fab.setOnClickListener { _ ->
            val fragmentNewCow: Fragment = fragment_new_cow()
            val fragment : Fragment? = supportFragmentManager.findFragmentByTag(fragment_new_cow::class.java.simpleName)
            if (fragment !is fragment_new_cow){
                val manager = supportFragmentManager.beginTransaction()
                manager.replace(R.id.FragmentContainerView, fragmentNewCow)
                manager.addToBackStack(null)
                manager.setReorderingAllowed(true)
                manager.commit()
            }

        }


    }



}
