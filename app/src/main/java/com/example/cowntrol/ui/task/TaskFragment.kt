package com.example.cowntrol.ui.task

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.example.cowntrol.R
import com.example.cowntrol.ui.APIService
import com.example.cowntrol.ui.TaskModel
import com.example.cowntrol.ui.globalUser
import com.example.cowntrol.ui.idUserModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class TaskFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_task, container, false)
        getTask(root)
        return root
    }

    private fun getTask(root : View?) {
        CoroutineScope(Dispatchers.Main).launch {
            val idUser = idUserModel(globalUser.id.toInt())
            val call : ArrayList<TaskModel> = getRetrofit().create(APIService::class.java).searchTareas(idUser) //prueba, cambiar al id del usuario login
            if(!call.isEmpty()){
                setData(call,root)
            }
        }
    }
    private fun setData(array : ArrayList<TaskModel>, root : View?) {

        //obtener la cantidad de solicitudes de la base de datos

        val size = array.size
        val buttonArray = ArrayList<Button>()
        val linearLayout = root?.findViewById<LinearLayout>(R.id.LinearLayoutft)
        //ciclo para crear los botones y llenar la view de ellos
        for (i in 1..size) {
            val button = Button(requireContext())
            button.setText("Tarea #" + array.get(i-1).id) // obtiene el id de la tarea
            if (array.get(i-1).estado == 2 || array.get(i-1).estado == 1 ){
                button.setBackgroundColor(ContextCompat.getColor(requireContext(),R.color.colorDeny))
            }
            if (array.get(i-1).estado == 3){
                button.setBackgroundColor(ContextCompat.getColor(requireContext(),R.color.colorPrimaryLow))
            }
            button.setOnClickListener() {
                openRequestFragment(array.get(i-1))
            }
            buttonArray.add(button)
            linearLayout?.addView(buttonArray.get(i-1))


        }

    }

    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl("http://159.223.204.114/api/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun openRequestFragment(tarea : TaskModel) {

        val datos : Bundle = Bundle()
        datos.putInt("id",tarea.id)
        datos.putInt("estado",tarea.estado)
        datos.putString("descripcion",tarea.descripcion)

        val fragmentTaskRegister: Fragment = TaskRegister()
        fragmentTaskRegister.arguments = datos
        val transaction: FragmentTransaction = requireFragmentManager().beginTransaction()
        transaction.replace(R.id.FragmentContainerView, fragmentTaskRegister)
        transaction.addToBackStack(null)
        transaction.setReorderingAllowed(true)
        transaction.commit()
    }
}