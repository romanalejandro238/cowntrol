package com.example.cowntrol.ui

data class VacasModel( //adaptar modelo a la api web
    val id : Int,
    val codigoVaca : String ,
    val pesoKgs : Number,
    val idRaza : String,
    val fechaNac : String,
    val fechaMuerte : String,
    val viva: Number, // 0 no 1 si
    val razaName : String,
    val ultimaInseminacion : String,
    val prenada : Number, // 0 no 1 si
    val raza : RazaModel
    )


