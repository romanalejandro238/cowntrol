package com.example.cowntrol.ui.ui.login

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.cowntrol.MainActivity
import com.example.cowntrol.databinding.ActivityLoginBinding
import com.example.cowntrol.ui.APIService
import com.example.cowntrol.ui.LoggedUser
import com.example.cowntrol.ui.LoginModel
import com.example.cowntrol.ui.globalUser
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class LoginActivity : AppCompatActivity() {

    private lateinit var loginViewModel: LoginViewModel
    private lateinit var binding: ActivityLoginBinding
    private lateinit var idUser: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
        idUser = ""
        val username = binding.username
        val password = binding.password
        val login = binding.login
        val loading = binding.loading

        loginViewModel = ViewModelProvider(this, LoginViewModelFactory())
            .get(LoginViewModel::class.java)

        loginViewModel.loginFormState.observe(this@LoginActivity, Observer {
            val loginState = it ?: return@Observer

            // disable login button unless both username / password is valid
            login.isEnabled = loginState.isDataValid

            if (loginState.usernameError != null) {
                username.error = getString(loginState.usernameError)
            }
            if (loginState.passwordError != null) {
                password.error = getString(loginState.passwordError)
            }
        })

        loginViewModel.loginResult.observe(this@LoginActivity, Observer {
            val loginResult = it ?: return@Observer

            loading.visibility = View.GONE
            if (loginResult.error != null) {
                showLoginFailed(loginResult.error)
            }
            if (loginResult.success != null) {
                updateUiWithUser(loginResult.success)
            }
            setResult(Activity.RESULT_OK)

            //Complete and destroy login activity once successful
            finish()
        })

        username.afterTextChanged {
            loginViewModel.loginDataChanged(
                username.text.toString(),
                password.text.toString()
            )
        }

        password.apply {
            afterTextChanged {
                loginViewModel.loginDataChanged(
                    username.text.toString(),
                    password.text.toString()
                )
            }

            setOnEditorActionListener { _, actionId, _ ->
                when (actionId) {
                    EditorInfo.IME_ACTION_DONE ->
                        loginViewModel.login(
                            username.text.toString(),
                            password.text.toString()
                        )
                }
                false
            }

            login.setOnClickListener {
                loading.visibility = View.VISIBLE
                getUser(username.text.toString(),password.text.toString())
                //loginViewModel.login(username.text.toString(), password.text.toString())
            }

        }
    }

    private fun updateUiWithUser(model: LoggedInUserView) {


    }

    private fun showLoginFailed(@StringRes errorString: Int) {
        Toast.makeText(applicationContext, errorString, Toast.LENGTH_SHORT).show()
    }


    private fun checkedUser(){
        val bundle = Bundle()
        bundle.putString("idUsuario",idUser)
        var mContext:Context = this@LoginActivity
        val mainActivity = Intent(mContext,MainActivity::class.java)
        mainActivity.putExtras(bundle)
        finish()
        startActivity(mainActivity)

    }
    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl("http://159.223.204.114/api/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun getUser(username : String, password : String) {
        val loginmodel : LoginModel = LoginModel(username.toInt(),password)

        CoroutineScope(Dispatchers.Main).launch {
            try{
                val call : LoggedUser = getRetrofit().create(APIService::class.java).searchLogin(loginmodel) // verificar que recupere el id
                guarda(call.id.toString())
                checkedUser()
            }catch (e : Exception){
                Toast.makeText(
                    applicationContext,
                    "DATOS ERRONEOS ",
                    Toast.LENGTH_LONG).show()
                loading.visibility = View.INVISIBLE
            }

        }

    }

    private fun guarda(toString: String) {
        idUser = toString
        globalUser.id = idUser

    }


}


/**
 * Extension function to simplify setting an afterTextChanged action to EditText components.
 */
fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(editable.toString())
        }

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
    })
}