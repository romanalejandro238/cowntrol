package com.example.cowntrol.ui

data class MilkModel(
    val fecha : String,
    val vacas : ArrayList<MilkCowModel>
)
