package com.example.cowntrol.ui.insemination

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.example.cowntrol.R
import com.example.cowntrol.ui.APIService
import com.example.cowntrol.ui.InseminacionModel
import com.example.cowntrol.ui.VacasModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.newFixedThreadPoolContext
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class InseminationFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?

    ): View? {
        val root = inflater.inflate(R.layout.fragment_insemination, container, false)
        getInseminations(root) // funcion para obtener registro de inseminaciones
        //creación de el nuevo fragmento apartir del Flotating Action Button
        val fab: View = root.findViewById(R.id.floatingActionButtonInsemination)
        fab.setOnClickListener { _ ->
            val fragmentNewInsemination: Fragment = fragment_new_insemination()
            val transaction: FragmentTransaction = requireFragmentManager().beginTransaction()
            transaction.replace(R.id.FragmentContainerView, fragmentNewInsemination)
            transaction.addToBackStack(null)
            transaction.setReorderingAllowed(true)
            transaction.commit()

        }
        return root
    }
    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl("http://159.223.204.114/api/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun getInseminations(root: View?) {
        //lista de inseminaciones

        CoroutineScope(Dispatchers.Main).launch {
            val call : ArrayList<VacasModel> = getRetrofit().create(APIService::class.java).searchVacas(); //esta llenando de vacas no inseminaciones
            setData(call,root)
            //HACER UN LLENADO DE VACAS EN LUGAR DE INSEMINACIONES Y AL DESPLEGAR LA INFORMACION DEL REGISTRO ENTONCES RECUPERAR SU INFORMACION

        }
    }

    private fun setData(array : ArrayList<VacasModel> , root : View?) {

        //obtener la cantidad de solicitudes de la base de datos
        val size = array.size
        val buttonArray = ArrayList<Button>()
        val linearLayout = root?.findViewById<LinearLayout>(R.id.LinearLayoutfi)
        //ciclo para crear los botones y llenar la view de ellos
        for (i in 1..size) {
            val button = Button(requireContext())
            if(array.get(i-1).ultimaInseminacion != ""){
                button.setText("Vaca #" + array.get(i-1).codigoVaca) // obtiene el codigo de la vaca
                button.setOnClickListener() {
                    openRequestFragment(array.get(i-1))
                }
                buttonArray.add(button)
                linearLayout?.addView(buttonArray.get(buttonArray.size-1))
            }
        }

    }


    private fun openRequestFragment(vaca : VacasModel) {

        //objeto tipo bundle para mandar información necesaria de la vaca
        val datos : Bundle = Bundle()
        datos.putString("idVaca",vaca.id.toString())
        datos.putString("codigoVaca",vaca.codigoVaca)
        datos.putString("ultInseminacion",vaca.ultimaInseminacion)

        val fragmentInseminationRegister: Fragment = InseminationRegister()
        fragmentInseminationRegister.arguments = datos
        val transaction: FragmentTransaction = requireFragmentManager().beginTransaction()
        transaction.replace(R.id.FragmentContainerView, fragmentInseminationRegister)
        transaction.addToBackStack(null)
        transaction.setReorderingAllowed(true)
        transaction.commit()
    }
}

