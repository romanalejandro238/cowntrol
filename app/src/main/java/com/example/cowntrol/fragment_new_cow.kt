package com.example.cowntrol

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.example.cowntrol.R
import com.example.cowntrol.ui.APIService
import com.example.cowntrol.ui.RazaModel
import com.example.cowntrol.ui.VacasModel
import com.example.cowntrol.ui.vacasEndPoint
import kotlinx.android.synthetic.main.fragment_new_cow.view.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import android.app.DatePickerDialog
import android.widget.DatePicker
import androidx.fragment.app.FragmentTransaction
import com.example.cowntrol.ui.insemination.InseminationFragment
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class fragment_new_cow : Fragment() , AdapterView.OnItemSelectedListener{

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }
    lateinit var adapter : ArrayAdapter<String>
    lateinit var razaId : String
    lateinit var arrayRazas : ArrayList<RazaModel>
    lateinit var fechaNac : String
    var btnFecha: Button? = null
    var cal = Calendar.getInstance()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_new_cow, container, false)

        val idVaca : EditText = root.findViewById(R.id.editTextidnc)
        val pesoVaca : EditText = root.findViewById(R.id.editTextwnc)
        btnFecha = root.findViewById(R.id.btnfDatenc)
        val razaVaca : Spinner = root.findViewById(R.id.spinnernc)

        val dateSetListener = object : DatePickerDialog.OnDateSetListener{
            override fun onDateSet(view: DatePicker, year : Int, monthOfyear : Int, dayOfMonth : Int){
                cal.set(Calendar.YEAR,year)
                cal.set(Calendar.MONTH,monthOfyear)
                cal.set(Calendar.DAY_OF_MONTH,dayOfMonth)
                updateDateInView()
            }
        }



        adapter = ArrayAdapter(requireContext(),R.layout.support_simple_spinner_dropdown_item)
        razaVaca.adapter = adapter
        razaVaca.onItemSelectedListener = this
        recuperaRazas()



        //si contiene elementos hace el llenado de razas


        val btnConfirm : Button = root.findViewById(R.id.btnncConfirm)
        val btnCancel : Button = root.findViewById(R.id.btnncCancel)

        btnConfirm.setOnClickListener(){
            confirmar(idVaca.text.toString(),pesoVaca.text.toString(),razaId,fechaNac)
        }
        btnCancel.setOnClickListener(){
            salir()
        }
        btnFecha!!.setOnClickListener(){
            DatePickerDialog(requireContext(),dateSetListener,cal.get(Calendar.YEAR),cal.get(Calendar.MONTH),cal.get(Calendar.DAY_OF_MONTH)).show()
        }


        return root
    }

    private fun selecionaFecha() {
        fechaNac = btnFecha?.text.toString()
    }

    private fun updateDateInView(){
        val myFormat = "yyyy/MM/dd"
        val sdf = SimpleDateFormat(myFormat, Locale.US)
        btnFecha!!.text = sdf.format(cal.getTime())
        selecionaFecha()

    }

    private fun recuperaRazas(){

        var call : ArrayList<RazaModel> = ArrayList<RazaModel>()
        CoroutineScope(Dispatchers.Main).launch {
            call = getRetrofit().create(APIService::class.java).searchRazas(); //recuperacion de razas
            setData(call)

        }
    }

    private fun setData(arrayRaza : ArrayList<RazaModel>) {
        arrayRazas = arrayRaza
        if(!arrayRazas.isEmpty()){
            for(i in 1.. arrayRazas.size){
                adapter.add(arrayRazas.get(i-1).descripcion)
            }
        }
    }

    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl("http://159.223.204.114/api/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun salir() {
        val fragmentInsemination: Fragment = InseminationFragment()
        val transaction: FragmentTransaction = requireFragmentManager().beginTransaction()
        transaction.replace(R.id.FragmentContainerView, fragmentInsemination)
        transaction.addToBackStack(null)
        transaction.setReorderingAllowed(true)
        transaction.commit()
    }

    private fun confirmar(idvaca : String, pesovaca : String, razaId : String ,fechaVaca : String) {
        //logica para subir a la base de datos la nueva vaca
        //hacer un llamado previo a searchRazas para recuperar el id de la raza

        CoroutineScope(Dispatchers.Main).launch {
            val vacasEnd : vacasEndPoint = vacasEndPoint(idvaca.toInt(),pesovaca.toInt(),fechaVaca, razaId.toInt())
            val call = getRetrofit().create(APIService::class.java).createVacas(vacasEnd) //recuperacion de razas
            Toast.makeText(requireContext(),"REGISTRADA CON EXITO",Toast.LENGTH_SHORT).show()
            salir()
        }
    }

    //recupera la raza seleccionada para enviar el id al crear nueva vaca
    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, posicion: Int, p3: Long) {
        var razaSelected = adapter.getItem(posicion)
        for (i in 1.. arrayRazas.size){
            if(arrayRazas.get(i-1).descripcion.equals(razaSelected)){
                razaId = arrayRazas.get(i-1).id.toString()
            }
        }
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {

    }
}

