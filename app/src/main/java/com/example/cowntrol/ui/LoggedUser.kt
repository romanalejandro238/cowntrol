package com.example.cowntrol.ui

data class LoggedUser(
    val id : Int,
    val nombre: String,
    val apellido_paterno : String,
    val apellido_materno : String,
    val codigoEmpleado:Int,
    val password:String,
    val idRol: Int,
    val activo:Int,
    val rolName:String

)
