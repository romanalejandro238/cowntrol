package com.example.cowntrol.ui.insemination

import android.app.DatePickerDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.FragmentTransaction
import com.example.cowntrol.R
import com.example.cowntrol.ui.APIService
import com.example.cowntrol.ui.InseminacionModel
import com.example.cowntrol.ui.VacasModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.text.SimpleDateFormat
import java.util.*


class InseminationRegister : Fragment() {

    lateinit var fechaAct : String
    var btnFecha: Button? = null
    var idVaca : Int = 0
    var cal = Calendar.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_insemination_register, container, false)
        if(arguments != null){
            idVaca = requireArguments().getString("idVaca")!!.toInt()
            val codigoVaca = requireArguments().getString("codigoVaca")
            val ultInseminacion = requireArguments().getString("ultInseminacion")

            val dateSetListener = object : DatePickerDialog.OnDateSetListener{
                override fun onDateSet(view: DatePicker, year : Int, monthOfyear : Int, dayOfMonth : Int){
                    cal.set(Calendar.YEAR,year)
                    cal.set(Calendar.MONTH,monthOfyear)
                    cal.set(Calendar.DAY_OF_MONTH,dayOfMonth)
                    updateDateInView()
                }
            }


            val editVacaId : EditText = root.findViewById(R.id.edittxtidfri)
            editVacaId.setText(codigoVaca)
            val editfechaAnt : EditText = root.findViewById(R.id.edittxtdatefri)
            editfechaAnt.setText(ultInseminacion)

            btnFecha = root.findViewById(R.id.btnnewdatefri)
            val prenada : CheckBox = root.findViewById(R.id.checkBoxfri)
            val btnUpdate : Button = root.findViewById(R.id.btnfriUpdate)
            btnUpdate.isEnabled = false
            val btnCancel : Button = root.findViewById(R.id.btnfriCancel)

            btnUpdate.setOnClickListener(){
                if(prenada.isChecked){
                    actualizarSuccess()
                }else{
                    actualizarInseminacion()
                }
            }

            btnCancel.setOnClickListener(){
                salir()
            }

            btnFecha?.setOnClickListener(){
                DatePickerDialog(requireContext(),dateSetListener,cal.get(Calendar.YEAR),cal.get(Calendar.MONTH),cal.get(Calendar.DAY_OF_MONTH)).show()
                btnUpdate.isEnabled = true
            }

        }







        return root
    }

    private fun actualizarSuccess() {
        //actualiza la inseminacion de la vaca con embarazo exitoso
        CoroutineScope(Dispatchers.Main).launch {
            val inseminacionModel = InseminacionModel(idVaca , fechaAct)
            getRetrofit().create(APIService::class.java).updateInseminacionSuccess(inseminacionModel) //esta llenando de vacas no inseminaciones
            Toast.makeText(requireContext(),"Inseminacion exitosa",Toast.LENGTH_SHORT).show()
            salir()


        }
    }

    private fun actualizarInseminacion() {
        //rutina para inseminar
        CoroutineScope(Dispatchers.Main).launch {
            val inseminacionModel = InseminacionModel(idVaca , fechaAct)
            getRetrofit().create(APIService::class.java).updateInseminacion(inseminacionModel) //esta llenando de vacas no inseminaciones
            Toast.makeText(requireContext(),"Actualizada",Toast.LENGTH_SHORT).show()
            salir()


        }
    }

    private fun salir() {
        val fragmentInsemination: Fragment = InseminationFragment()
        val transaction: FragmentTransaction = requireFragmentManager().beginTransaction()
        transaction.replace(R.id.FragmentContainerView, fragmentInsemination)
        transaction.addToBackStack(null)
        transaction.setReorderingAllowed(true)
        transaction.commit()
    }



    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl("http://159.223.204.114/api/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun updateDateInView(){
        val myFormat = "yyyy/MM/dd"
        val sdf = SimpleDateFormat(myFormat, Locale.US)
        btnFecha!!.text = sdf.format(cal.getTime())
        selecionaFecha()

    }

    private fun selecionaFecha() {
        fechaAct = btnFecha?.text.toString()
    }


}