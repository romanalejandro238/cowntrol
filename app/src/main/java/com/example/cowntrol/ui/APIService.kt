package com.example.cowntrol.ui


import retrofit2.http.*


interface APIService {

    //INICIO USUARIOS
    @POST("auth/login")
    suspend fun searchLogin(@Body loginModel: LoginModel): LoggedUser

    //FIN USUARIOS

    //INICIO VACAS
    @GET("vacas/index")
    suspend fun searchVacas(): ArrayList<VacasModel> //buscando vacas

    @GET("vacas/razas")
    suspend fun searchRazas(): ArrayList<RazaModel>

    @POST("vacas/create")
    suspend fun createVacas(@Body vacasEnd: vacasEndPoint) //creando vacas
    //FIN VACAS

    //inicio inseminacion
    @POST("vacas/inseminaciones") //recupera las inseminaciones
    suspend fun searchInseminaciones(): InseminacionModel

    @POST("vacas/inseminacion")
    suspend fun updateInseminacion(@Body inseminacionModel: InseminacionModel) //parametros para actualizar

    @POST("vacas/inseminacion")
    suspend fun createInseminacion(@Body inseminacionModel: InseminacionModel) //parametros para crear

    @POST("vacas/inseminacionSuccess")
    suspend fun updateInseminacionSuccess(@Body inseminacionModel: InseminacionModel) // aun no la confirma como


    suspend fun deleteInseminacion () //parametros para eliminar


    //fin inseminacion



    //inicio leche
    @POST("vacas/guardarLeche")
    suspend fun createUpdateLeche (@Body milkModel: MilkModel)




    //fin leche




    //inicio tareas
    @POST("tareas/index")
    suspend fun searchTareas(@Body idUsuario : idUserModel) : ArrayList<TaskModel> //recibe parametros para buscar tareas del usuario


    @POST("tareas/cambiarEstatus")
    suspend fun updateTareas(@Body task : TaskModel) //parametros para actualizar


    //fin tareas
}