package com.example.cowntrol.ui

data class vacasEndPoint( val codigoVaca : Int,
                          val pesoKgs : Int,
                          val fechaNac : String,
                          val idRaza : Int
                          )
